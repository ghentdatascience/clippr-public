import numpy as np
import tensorflow as tf
from pymanopt.manifolds import Stiefel
from pymanopt.solvers import SteepestDescent
from scipy.optimize import minimize
from pymanopt import Problem

def computeW_circle(X, k, f, sigma, W0):
    n, d = X.shape
    # manopt init: manifold, solver
    manifold = Stiefel(d,k)
    # solver = SteepestDescent(minstepsize=1e-12, logverbosity=2)
    solver = SteepestDescent(logverbosity=2)

    # specify TF variables
    W = tf.Variable(tf.placeholder(tf.float64, shape=(d, k)))
    XW = tf.matmul(X,W)

    Icardl = tf.constant(np.array(range(n,0,-1)), tf.float64)
    Icardr = tf.constant(np.array(range(n-1,-1,-1)), tf.float64)

    z = tf.norm(XW, axis=1)
    r = tf.gather(z, tf.nn.top_k(z, k=n).indices)
    rsquare = r**2/(2*sigma**2.)

    objl = tf.add(tf.cumsum(rsquare, reverse=True),
    tf.add(tf.multiply(n-Icardl, np.log(2*np.pi) + rsquare),
    tf.add(Icardl*np.log(2*np.pi*sigma**2),
    tf.multiply(n-3*Icardl, tf.log(2*r*f)))))

    objr = tf.add(tf.cumsum(rsquare, reverse=True, exclusive=True),
    tf.add(tf.multiply(n-Icardr, np.log(2*np.pi) + rsquare),
    tf.add(Icardr*np.log(2*np.pi*sigma**2),
    tf.multiply(n-3*Icardr,tf.log(2*r*f)))))

    rv = tf.maximum(objl,objr)
    objective = tf.reduce_max(rv)

    problem = Problem(manifold = manifold, cost = -objective, arg=W, verbosity=0)

    Wopt, optlog = solver.solve(problem, x = W0)
    sess = tf.Session()
    val = sess.run(objective, {W: Wopt})
    sess.close()

    sess = tf.Session()
    [rs, rvs, inds, objls, objrs] = sess.run([r, rv, tf.nn.top_k(z, k=n).indices, objl, objr], {W: Wopt})
    sess.close()

    idx = np.argmax(rvs)
    if objls[idx] < objrs[idx]:
        I = np.array(inds)[(idx+1):]
    else:
        I = np.array(inds)[idx:]
    radius = rs[idx]

    return Wopt, val, radius

def computeW_rectangle(X, k, f, sigma, W0):
    n, d = X.shape
    # manopt init: manifold, solver
    manifold = Stiefel(d,k)
    # solver = SteepestDescent(minstepsize=1e-12, logverbosity=2)
    solver = SteepestDescent(logverbosity=2)

    # specify TF variables
    W = tf.Variable(tf.placeholder(tf.float64, shape=(d, k)))
    XW = tf.matmul(X,W)

    objValues = []
    Cs = []
    CVs = []
    Inds = []
    Objls = []
    Objrs = []
    Icardl = tf.constant(np.array(range(n,0,-1)), tf.float64)
    Icardr = tf.constant(np.array(range(n-1,-1,-1)), tf.float64)
    for j in range(k):
        Zabs = abs(tf.slice(XW, [0,j], [n,1]))[:,0]
        C = tf.gather(Zabs, tf.nn.top_k(Zabs, k=n).indices)
        Zsquare = C**2/(2*sigma**2.)

        objl = tf.add(tf.cumsum(Zsquare, reverse=True),
        tf.add(tf.multiply(n-Icardl, Zsquare),
        tf.add(tf.multiply(n-2*Icardl, tf.log(C)),
        Icardl*(0.5*np.log(2*np.pi*sigma**2) - np.log(2*f))
        + (n-Icardl)*np.log((2*np.pi)**.5/sigma))))

        objr = tf.add(tf.cumsum(Zsquare, reverse=True, exclusive=True),
        tf.add(tf.multiply(n-Icardr, Zsquare),
        tf.add(tf.multiply(n-2*Icardr, tf.log(C)),
        Icardr*(0.5*np.log(2*np.pi*sigma**2) - np.log(2*f))
        + (n-Icardr)*np.log((2*np.pi)**.5/sigma))))

        obj = tf.maximum(objl,objr)

        objValues.append(tf.reduce_max(obj))
        # for plotting
        Cs.append(C)
        CVs.append(obj)
        Inds.append(tf.nn.top_k(Zabs, k=n).indices)
        Objls.append(objl)
        Objrs.append(objr)
    objective = tf.reduce_sum(objValues)

    problem = Problem(manifold = manifold, cost = -objective, arg=W, verbosity=0)

    Wopt, optlog = solver.solve(problem, x = W0)
    sess = tf.Session()
    val = sess.run(objective, {W: Wopt})
    sess.close()

    sess = tf.Session()
    [cs, cvs, inds, objls, objrs] = sess.run([Cs, CVs, Inds, Objls, Objrs], {W: Wopt})
    sess.close()

    Is = []
    optCs = []
    for i in range(k):
        idx = np.argmax(cvs[i])
        if objls[i][idx] < objrs[i][idx]:
            Is.append(np.array(inds[i])[(idx+1):])
        else:
            Is.append(np.array(inds[i])[idx:])
        optCs.append(cs[i][idx])

    return Wopt, val, optCs, Is

def func(ab):
    return -np.log(ab[0]) - np.log(ab[1])

def func_deriv(ab):
    return np.array([ -1/ab[0], -1/ab[1] ])

def negLogProb(X, W, a, b, sigma, f):
    XW = tf.matmul(X, W)
    z = tf.squeeze(tf.matmul(XW**2, np.array([a,b])[:,None]))

    inI = z <= 1.0
    outI = z > 1.0
    inXW = tf.boolean_mask(XW, inI)
    outXW = tf.boolean_mask(XW, outI)
    outZ = tf.boolean_mask(z, outI)

    inVals = np.log(2*np.pi*sigma**2) + tf.reduce_sum(inXW**2, axis=1)/(2*sigma**2) - 2*np.log(2*f) + np.log(a*b)/2
    outVals = -np.log(f/np.pi) + tf.div(tf.reduce_sum(outXW**2, axis=1), outZ)/(2*sigma**2)

    return tf.reduce_sum(inVals) + tf.reduce_sum(outVals)

def evalNegLogProb(X, W, a, b, sigma, f):
    XW = np.dot(X,W)
    z = np.dot(XW**2, [a,b])
    inI = z <= 1.0
    outI = z > 1.0
    inXW = XW[inI,]
    outXW = XW[outI,]
    outZ = z[outI]

    inVals = np.log(2*np.pi*sigma**2) + np.sum(inXW**2, axis=1)/(2*sigma**2) - 2*np.log(2*f) + np.log(a*b)/2
    outVals = -np.log(f/np.pi) + np.sum(outXW**2,axis=1)/outZ/(2*sigma**2)

    return np.sum(inVals) + np.sum(outVals)

def findW(X, a, b, k, sigma, f, W0):
    n,d  = X.shape
    # manopt init: manifold, solver
    manifold = Stiefel(d,k)
    # solver = SteepestDescent(minstepsize=1e-12, logverbosity=2)
    solver = SteepestDescent(logverbosity=2)
    # specify TF variables
    W = tf.Variable(W0,tf.float64)

    objective = negLogProb(X, W, a, b, sigma, f)

    problem = Problem(manifold = manifold, cost = -objective, arg=W, verbosity=0)

    Wopt, optlog = solver.solve(problem, x = W0)
    sess = tf.Session()
    val = sess.run(objective, {W: Wopt})
    sess.close()

    return Wopt, val

def findAB(X, W, a, b, sigma, f):
    optA = a;
    optB = b
    optObj = evalNegLogProb(X, W, optA, optB, sigma, f)
    oldObj = optObj
    XW = np.dot(X, W)
    numIter = 10
    for i in range(numIter):
        if i%2 == 1:
            testAs = (1-optB*XW[:,1]**2)/XW[:,0]**2
            validAs = testAs[testAs > 0]
            objs = [evalNegLogProb(X, W, testA, optB, sigma, f) for testA in validAs]
            maxIdx = np.argmax(objs);
            maxObj = objs[maxIdx]
            if maxObj > optObj:
                optObj = maxObj
                optA = validAs[maxIdx]
        else:
            testBs = (1-optA*XW[:,0]**2)/XW[:,1]**2
            validBs = testBs[testBs > 0]
            objs = [evalNegLogProb(X,W, optA, testB, sigma, f) for testB in validBs]
            maxIdx = np.argmax(objs);
            maxObj = objs[maxIdx]
            if maxObj > optObj:
                optObj = maxObj
                optB = validBs[maxIdx]

        if abs(oldObj - optObj) < np.finfo('float32').eps:
            break
        oldObj = optObj
    return optA, optB



def computeW_ellipse(X, k, f, sigma, W0):
    n, d = X.shape
    W = W0
    XW = np.dot(X, W)

    # compute initial box that tightly fits the data.
    constraints = []
    for i in range(0, n):
        constraints.append({'type': 'ineq', 'fun' : lambda ab: -np.dot(XW[i, ]**2, ab) + 1, 'jac' : lambda ab: -XW[i, ]**2})
    constraints_ = tuple(constraints)
    minNorm = min(np.sum(XW**2,axis=1))**.5
    ab0 = [minNorm,minNorm]
    bound = (np.finfo('float32').eps, np.finfo('float32').max)
    res = minimize(func, ab0, jac=func_deriv, method='SLSQP', options={'disp': None, 'maxiter': 400}, constraints=constraints_, bounds=[bound, bound])
    ab = res.x

    ## alterantive optimization
    optA = ab[0]
    optB = ab[1]

    currObj = evalNegLogProb(X, W, optA, optB, sigma, f)
    oldObj = currObj
    # print('start obj = {}'.format(currObj))
    numIter = 10
    for i in range(numIter):
        # print('iteration: {}'.format(i))
        W, val = findW(X, optA, optB, k, sigma, f, W)
        # print('This should be zero: {}'.format(val - evalNegLogProb(X, W, optA, optB, sigma, f)))
        optA, optB = findAB(X, W, optA, optB, sigma, f)
        currObj = evalNegLogProb(X, W, optA, optB, sigma, f)
        # print(currObj)
        if abs(oldObj - currObj) < np.finfo('float32').eps:
            break
        oldObj = currObj
    # print('end obj = {}'.format(currObj))
    XW = np.dot(X, W)
    return W, val, optA, optB
